# 海康+vue项目

#### 介绍
基于vuecli2.x项目 调用海康摄像头的基本功能

解释说明: 
    1. 上传的几个文件,是从vue项目中截取出来的, 用到的开发者 需要移植到自己的项目中对应的位置
    2. index.html中的script标签引用的位置, 需要在body之后,否则会报错
    3. hkTest.vue文件为项目中组件
    4. jsPlugin-1.2.0.min.js 与 webVideoCtrl.js 文件 需要放到vue项目中的 static目录中且两文件同级
    5. 关于vue项目中的.babelrc 需要配置, 具体写法看图
    6. 关于package.json文件, 用到的x2js插件 与 element
    7. 用到了JQ 自行下载

关于webVideoCtrl文件:
    从网上找了很多,但是都报错,当前上传的webVideoCtrl.js文件运行无问题

关于海康:
    当前项目是海康的3.2版本(无插件版本) 

关于浏览器
    用的是IE11

************ 如果能帮助到你, 我很高兴 ***************